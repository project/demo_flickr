; demo_flickr make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[responsive_bartik][type] = theme
projects[responsive_bartik][download][type] = git
projects[responsive_bartik][download][revision] = fc972f24d94be3805a7ae503f44e2f344db1ee68
projects[responsive_bartik][download][branch] = 7.x-1.x

projects[flickr][version] = "1.x-dev"
projects[flickr][subdir] = "contrib"

projects[autofloat][version] = "2.x-dev"
projects[autofloat][subdir] = "contrib"

projects[colorbox][version] = "2.8"
projects[colorbox][subdir] = "contrib"

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

; +++++ Libraries +++++

; ColorBox
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"
libraries[colorbox][destination] = "libraries"
libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.x.zip"

